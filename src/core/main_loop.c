#include "main_loop.h"

#include "main.h"
#include "stm32g4xx_hal.h"

void main_loop() {
	HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
	HAL_Delay(250);
}
